
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class MysqlConnect {
    Connection conn;

    /*
    returns a vaalid Connection obj if it can establish connection with empdb
    else returns null
    */  
    public static Connection connectDB(){
        try{
            Connection conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/qp_generator","isha","isha");
//            JOptionPane.showMessageDialog(null,"connection established sucessfully");
            return conn;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,"connection failed"+e);
            return null;
        }
    }  
}
